<?php

require_once 'autoload.php';
require_once 'app/flight/Flight.php';
require_once 'config.php';

session_start();

$authController = new AuthController;
Flight::route( '/authentication',   [$authController, 'form' ] );
Flight::route( 'POST /login',       [$authController, 'login' ] );
Flight::route( 'POST /signin',      [$authController, 'signin' ] );
Flight::route( '/signout',          [$authController, 'signout' ] );

$chamberController = new ChamberController;
Flight::route( '/',                     [$chamberController, 'index'] );
Flight::route( '/chamber/@id:[0-9]*',   [$chamberController, 'show'] );
Flight::route( 'GET /chamber/create',   [$chamberController, 'showCreate'] );
Flight::route( 'POST /chamber/create',  [$chamberController, 'create'] );
Flight::route( '/chamber/list',         [$chamberController, 'showList'] );

$reserveController = new ReserveController;
Flight::route( 'POST /chamber/booking/@id[0-9]*',   [$reserveController, 'reserve'] );
Flight::route( '/chamber/booking',                  [$reserveController, 'show'] );

$favoriteController = new FavoriteController;
Flight::route( '/chamber/favorite',                 [$favoriteController, 'show'] );
Flight::route( 'POST /chamber/favorite/check/@id[0-9]*',  [$favoriteController, 'addFav'] );

Flight::start();