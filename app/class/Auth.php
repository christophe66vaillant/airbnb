<?php

class Auth
{
    private $username;
    private $password;
    private $password_check;

    public function __construct($username, $password, $password_check = '')
    {
        $this->username = $username;
        $this->password = hash('sha256', $password);
        $this->password_check = hash('sha256', $password_check);
    }

    public function login() : ? User
    {
        $sql = 'SELECT * FROM users WHERE username=:username AND password=:password';

        $stmt = (new Bdd)->getPdo()->prepare($sql);
        $stmt->execute(['username' => $this->username, 'password' => $this->password]);

        $result = $stmt->fetch();

        if (!$result) return null;

        $user = new User($result);
        $_SESSION['user'] = $user;
        
        return $user;
    }

    public function checkPasswords(): bool
    {
        return $this->password == $this->password_check;
    }

    public function userExist(): bool
    {
        $sql = 'SELECT id FROM users WHERE username=:username';
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute(['username' => $this->username]);

        return $stmt->rowCount() > 0;
    }

    public function signin( $role ): ?User
    {
        $user = new User;
        $user->setUsername( $this->username )->setPassword( $this->password )->setRole_id( $role );

        if( $user->signin() ){
            $_SESSION[ 'user' ] = $user;
            return $user;
        }else return null;
    }

    public static function isLogged(): bool
    {
        return isset( $_SESSION[ 'user' ] );
    }

    public static function user(): ?User
    {
        return self::isLogged() ? $_SESSION[ 'user' ] : null;    
    }

}