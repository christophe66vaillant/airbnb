<?php

class HousingTypes
{
    public static function findAll()
    {
        $sql = 'SELECT * FROM housing_types';

        $results = (new Bdd)->getPdo()->query( $sql )->fetchAll();
        
        $housings = [];
        foreach ($results as $result) {
            $housing = $result;

            $housings[] = $housing;
        }
        return $housings;
    }
}