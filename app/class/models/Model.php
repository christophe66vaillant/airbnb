<?php 
abstract class Model {

    protected $id;

    protected $bdd;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function __construct( array $datas = [] ) {
        
        $this->bdd = new Bdd;
        $this->hydrate( $datas );

    }

    private function hydrate( array $datas ) {
        
        foreach( $datas as $key => $data ) {

            $setter = 'set' . ucfirst($key);

            if( method_exists( $this, $setter ) ) {

                $this->{$setter}( $data );

            }
            
        }

    }

}
