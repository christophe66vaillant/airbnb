<?php

class Picture extends Model
{
    private $valid_ext = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'ico'];
    
    public function getValid_ext()
    {
        return $this->valid_ext;
    }

    public static function inArray()
    {
        $upload_ext = strtolower(substr(strrchr($_FILES[ 'image'][ 'name' ], '.'), 1));
        return in_array( $upload_ext, (new Picture)->getValid_ext() );
    }

    public static function stockImage( $chamber_id )
    {
        if( $_FILES[ 'images' ][ 'size' ] > 500000 ){
            $pictures = [];

            foreach( $_FILES[ 'images' ]['name'] as $picture => $val){
                $newFileName = password_hash( $picture[0], PASSWORD_BCRYPT ) . '.' . $val;
                $newFileName = ($newFileName = str_replace( '/', '£', $newFileName ));
                $targetFile = 'public/assets/images/' . $newFileName;
                $picture = move_uploaded_file( $_FILES[ 'images' ][ 'tmp_name' ][$picture], $targetFile );

                $pictures[] = $newFileName;
            }
            $image = (new Picture)->linkPictures( $pictures, $chamber_id );
        }
    }

    public static function linkPictures( array $pictures, int $chamber_id )
    {
        $sql = 'INSERT INTO chamber_images VALUES ';
        foreach ($pictures as $picture) {
            $sql .= "(0,  {$chamber_id}, '{$picture}', abs(rand() * 10000) ), ";
        }
        $sql = rtrim( $sql, ', ' );
        $stmt = (new Bdd)->getPdo()->query( $sql );

        return true;
    }

    public static function findAllById( $chamber_id )
    {
        $sql = 'SELECT * FROM chamber_images WHERE chamber_id = :chamber_id';
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute(['chamber_id' => $chamber_id]);

        $results = $stmt->fetchAll();

        $pictures = [];

        foreach ($results as $result) {
            $picture = $result;

            $pictures[] = $picture;
        }

        return $pictures;
    }
}