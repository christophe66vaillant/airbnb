<?php

class Reserve extends Model
{
    public function create( $chamber_id,  $start_time, $end_time )
    {
        $sql = 'INSERT INTO reserved VALUES (:chamber_id, :user_id, :start_time, :end_time)';

        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute([
            'chamber_id' => $chamber_id, 
            'user_id' => $_SESSION[ 'user' ]->getId(), 
            'start_time' => $start_time, 
            'end_time'=> $end_time]);
        
        return $stmt->rowCount() > 0;
    }

    public static function findAllByOwnerId($owner_id)
    {
        if( $_SESSION[ 'user' ]->getRole_id() == 1 ){
            $sql = "SELECT users.username, chambers.*, reserved.*, 
                DATE_FORMAT(start_time, ' %d %M %Y à %H heure %i' ) AS start_t,
                DATE_FORMAT(end_time, '  %d %M %Y à %H heure %i') AS end_t
                FROM reserved 
                JOIN chambers 
                ON chambers.id = reserved.chamber_id
                JOIN users
                ON users.id = reserved.user_id
                WHERE chambers.owner_id= :owner_id
                ORDER BY start_t";
        }else {
            $sql = "SELECT users.username, chambers.*, reserved.*, addresses.*, 
                DATE_FORMAT(start_time, ' %d %M %Y à %H heure %i' ) AS start_t,
                DATE_FORMAT(end_time, ' %d %M %Y à %H heure %i') AS end_t
                FROM reserved 
                JOIN chambers 
                ON chambers.id = reserved.chamber_id
                JOIN users
                ON users.id = reserved.user_id
                JOIN addresses
                ON addresses.id = chambers.address_id
                WHERE reserved.user_id= :owner_id
                ORDER BY start_t";
        }
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute( ['owner_id' => $owner_id] );

        $results = $stmt->fetchAll();

        $reserved = [];

        foreach ($results as $result) {
            $reserve = $result;

            $reserved[] = $reserve;
        }
        return $reserved;
    }

}