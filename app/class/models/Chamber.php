<?php

class Chamber extends Model
{
    private $label;
    private $price;
    private $bedrooms;
    private $size;
    private $string_desc;
    private $housing_label;
    private $description;
    private $country;
    private $city;
    private $equipement_label;
    private $address_id;
    private $equipements = [];


    #region Getters

    public function getAddress_id()
    {
        return $this->address_id;
    }

    public function getEquipement_label()
    {
        return $this->equipement_label;
    }

    public function getImg()
    {
        return $this->img;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getHousing_label()
    {
        return $this->housing_label;
    }

    public function getBedrooms()
    {
        return $this->bedrooms;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getString_desc()
    {
        return $this->string_desc;
    }

    #endregion

    #region Setters

    public function setAddress_id($address_id)
    {
        $this->address_id = $address_id;

        return $this;
    }

    public function setEquipement_label($equipement_label)
    {
        $this->equipement_label = $equipement_label;

        return $this;
    }

    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function setHousing_label($housing_label)
    {
        $this->housing_label = $housing_label;

        return $this;
    }

    public function setBedrooms($bedrooms)
    {
        $this->bedrooms = $bedrooms;

        return $this;
    }

    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    public function setString_desc($string_desc)
    {
        $this->string_desc = $string_desc;

        return $this;
    }

    #endregion

    public function create( Array $array )
    {
        $pdo = (new Bdd)->getPdo();
        $sql = "INSERT INTO chambers VALUES (0, :label, :size, :bedrooms, :description, :price, :housing_id, :address_id, :owner_id)";

        $stmt = $pdo->prepare( $sql );
        $stmt->execute( [
            'label' => $array['label'],
            'size' => $array['size'], 
            'bedrooms' => $array['bed'], 
            'description' => $array['description'], 
            'price' => $array['price'],
            'housing_id' => $array['housing_type'],
            'address_id' => $this->address_id,
            'owner_id' => $_SESSION[ 'user' ]->getId()
        ] );
        $stmt->fetch();
        
        if( $stmt->rowCount() > 0){
            $this->id = $pdo->lastInsertId();
            $sql2 = "UPDATE chamber_images SET image=NULL WHERE image LIKE '%.'";
            $stmt = $pdo->query( $sql2 );
            return true;
        }else return false;
    }

    public function createAddress( string $country, string $city ): bool
    {
        $pdo = (new Bdd)->getPdo();
        $sql = 'INSERT INTO addresses VALUES (0, :country, :city)';
        $stmt = $pdo->prepare( $sql );
        $stmt->execute( ['country' => $country, 'city' => $city ] );

        if($stmt->rowCount() > 0){
            $this->address_id = $pdo->lastInsertId();
            return true;
        }else return false;
    }

    public function linkEquipements( array $equipements ): bool
    {
        $sql = 'INSERT INTO equipement_chamber VALUES ';
        foreach ($equipements as $equipement) {
            $sql .= "( {$equipement}, {$this->id} ), ";
        }
        $sql = rtrim( $sql, ', ' );

        $stmt = $this->bdd->getPdo()->query( $sql );
        
        return $stmt->rowCount() > 0;
    }

    public static function findAllByOwnerId( int $owner_id )
    {
        $sql = "SELECT DISTINCT chambers.*, 
            addresses.country, 
            addresses.city,
            group_concat(equipements.label SEPARATOR ', ') AS equipement_label,
            housing_types.label AS housing_label 
            FROM chambers 
            LEFT JOIN housing_types 
            ON housing_types.id = chambers.housing_id 
            LEFT JOIN addresses
            ON addresses.id = chambers.address_id
            LEFT JOIN equipement_chamber
            ON equipement_chamber.chamber_id = chambers.id
            LEFT JOIN equipements
            ON equipement_chamber.equipement_id = equipements.id
            WHERE owner_id = :owner_id
            GROUP BY chambers.id";
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute([ 'owner_id' => $owner_id ]);

        $results = $stmt->fetchAll();

        $chambers = [];

        foreach ($results as $result) {
            $chamber = new Chamber( $result );

            $chambers[] = $chamber;
        }

        return $chambers;
    }

    public static function findAllById( int $chamber_id )
    {
        $sql = "SELECT DISTINCT chambers.*, 
            addresses.country, 
            addresses.city,
            group_concat(equipements.label SEPARATOR '</p> ') AS equipement_label,
            housing_types.label AS housing_label, 
            SUBSTRING(description, 1, 200) AS string_desc
            FROM chambers 
            JOIN housing_types 
            ON housing_types.id = chambers.housing_id 
            JOIN addresses
            ON addresses.id = chambers.address_id
            JOIN equipement_chamber
            ON equipement_chamber.chamber_id = chambers.id
            JOIN equipements
            ON equipement_chamber.equipement_id = equipements.id
            WHERE chambers.id = :chamber_id";
        $stmt = (new Bdd)->getPdo()->prepare($sql);
        $stmt->execute(['chamber_id' => $chamber_id]);

        $results = $stmt->fetchAll();

        $chambers = [];

        foreach ($results as $result) {
            $chamber = new Chamber( $result );

            $chambers[] = $chamber;
        }

        return $chambers;
    }

    public static function findALl( $select_id = ''  )
    {
        $sql = "SELECT DISTINCT chambers.*, 
            addresses.country,
            group_concat(equipements.label SEPARATOR ', ') AS equipement_label,
            housing_types.label AS housing_label, 
            SUBSTRING(description, 1, 200) AS string_desc
            FROM chambers
            JOIN housing_types 
            ON housing_types.id = chambers.housing_id
            JOIN addresses
            ON addresses.id = chambers.address_id
            LEFT OUTER JOIN equipement_chamber
            ON equipement_chamber.chamber_id = chambers.id
            LEFT OUTER JOIN equipements
            ON equipement_chamber.equipement_id = equipements.id
            GROUP BY chambers.id";
        if( $select_id !== '' ){
            $sql .= " ORDER BY {$select_id}";
            $results = (new Bdd)->getPdo()->query($sql)->fetchAll();
        }else{
            $results = (new Bdd)->getPdo()->query( $sql )->fetchAll();
        }

        $chambers = [];

        foreach ($results as $result) {
            $chamber = new Chamber( $result );

            $chambers[] = $chamber;
        }

        return $chambers;
    }

}