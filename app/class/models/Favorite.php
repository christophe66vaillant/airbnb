<?php

class Favorite extends Model
{
    public static function findAllByUserId( $user_id )
    {
        $sql = 'SELECT chambers.id, chambers.label, chambers.price, addresses.country, addresses.city
            FROM user_chambers 
            JOIN chambers 
            ON chambers.id = user_chambers.chamber_id
            JOIN addresses
            ON addresses.id = chambers.address_id
            WHERE user_chambers.user_id = :user_id';
        
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute([ 'user_id' => $user_id ]);

        $results = $stmt->fetchAll();

        $favorites = [];

        foreach ($results as $result) {
            $favorite = $result;

            $favorites[] = $favorite;
        }

        return $favorites;
    }

    public static function checkFav( $user_id, $chamber_id )
    {
        if( Favorite::findAllByUserAndChamberId( $user_id, $chamber_id ) ){
            $sql = 'DELETE FROM user_chambers WHERE user_id = :user_id AND chamber_id = :chamber_id';
        }else{
            $sql = 'INSERT INTO user_chambers VALUES( :user_id, :chamber_id)';
        }
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute(['user_id' => $user_id, 'chamber_id' => $chamber_id]);
        return $stmt->rowCount() > 0;
    }

    public static function findAllByUserAndChamberId( $user_id, $chamber_id )
    {
        $sql = 'SELECT chambers.id
            FROM chambers 
            JOIN user_chambers 
            ON chambers.id = user_chambers.chamber_id
            JOIN users
            ON users.id = user_chambers.user_id
            WHERE user_chambers.user_id = :user_id
            AND chambers.id = :chamber_id';
        
        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute([ 'user_id' => $user_id, 'chamber_id' => $chamber_id ]);

        return $stmt->rowCount() > 0;
    }
}