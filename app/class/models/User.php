<?php

class User extends Model
{
    private $username;
    private $password;
    private $role_id;

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function setRole_id($role_id)
    {
        $this->role_id = $role_id;

        return $this;
    }

    public function hasRole( string $role ): ?bool
    {
        $sql = 'SELECT role_id FROM users WHERE id=:id';
        $stmt = $this->bdd->getPdo()->prepare( $sql );
        $stmt->execute( ['id' => $this->id] );

        $result = $stmt->fetch();

        return $role == $result[ 'role_id' ];
        
    }

    public function signin(): bool
    {
        $pdo = $this->bdd->getPdo();
        $sql = 'INSERT INTO users VALUES (0, :username, :password, :role_id)';
        $stmt = $pdo->prepare( $sql );
        $stmt->execute(['username' => $this->username, 'password' => $this->password, 'role_id' => $this->role_id]);

        if( $stmt->rowCount() == 0 ){
            return false;
        }
        $this->id = $pdo->lastInsertId();
        return true;
    }

    public static function findAllRoles()
    {
        $sql = 'SELECT * FROM roles';
        $results = (new Bdd)
            ->getPdo()
            ->query( $sql )
            ->fetchAll();

        $roles = [];

        foreach ($results as $result) {
            $role = $result;

            $roles[] = $role;
        }

        return $roles;
    }

    /**
     * Get the value of role_id
     */ 
    public function getRole_id()
    {
        return $this->role_id;
    }
}