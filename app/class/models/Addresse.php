<?php 

class Addresse extends Model
{
    public static function getCountries()
    {
        $sql = 'SELECT id, country FROM addresses GROUP BY country';
        $stmt = (new Bdd)->getPdo()->query($sql);
        $results = $stmt->fetchAll();

        $countries = [];

        foreach ($results as $result) {
            $country = $result;

            $countries[] = $country;
        }
        return $countries;
    }

    public static function getCities($country_name)
    {
        $sql = "SELECT id, city FROM addresses WHERE country=:country_name";
        $stmt = (new Bdd)->getPdo()->prepare($sql);
        $stmt->execute(['country_name' => $country_name]);
        $results = $stmt->fetchAll();

        $cities = [];

        foreach ($results as $result) {
            $city = $result;

            $cities[] = $city;
        }
        return $cities;
    }
}