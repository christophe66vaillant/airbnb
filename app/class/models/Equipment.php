<?php 

class Equipment extends Model
{
    private $equipements;
    private $name;
    private $description;

    #region Getters

    public function getDescription()
    {
        return $this->description;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function getEquipements()
    {
        return $this->equipements;
    }

    #endregion

    #region Setters

    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
    
    public function setEquipements($equipements)
    {
        $this->equipements = $equipements;

        return $this;
    }

    #endregion

    // public function getCategories(): array
    // {
    //     if( empty( $this->equipements ) ){
    //         $sql = 'SELECT equipements.* 
    //             FROM equipements
    //             JOIN equipement_chamber as ec
    //             ON ec.equipement_id = equipements.id
    //             WHERE ec.chamber_id = :id';

    //         $stmt = $this->bdd->getPdo()->prepare( $sql );
    //         $stmt->execute( [ 'id' => $this->id ] );
    //         $results = $stmt->fetchAll();

    //         foreach ($results as $result) {
    //             $category = new Category( $result );
    //             $this->addCategory( $category );
    //         }
    //     }
    //     return $this->equipements;
    // }

    // public function addEquipements( Category $equipement )
    // {
    //     $this->equipements[] = $equipement;
    // }

    // public function hasEquipement( Category $equipement ): bool
    // {
    //     foreach ($this->getEquipements() as $own_equipement) {
    //         if( $own_equipement->getId() === $equipement->getId() ) return true;
    //         }
    //         return false;
    // }

    // Link category at his product in the BDD


    public static function findAll(): array
    {
        $sql = 'SELECT * FROM equipements';
        $results = (new Bdd)
            ->getPdo()
            ->query( $sql )
            ->fetchAll();

        $equipements = [];

        foreach ($results as $result) {
            $equipement = $result;

            $equipements[] = $equipement;
        }

        return $equipements;
    }

    public static function findAllById( int $chamber_id )
    {
        $sql = 'SELECT DISTINCT equipements.label AS name 
            FROM equipement_chamber
            JOIN chambers
            ON chambers.id = equipement_chamber.chamber_id
            JOIN equipements
            ON equipements.id = equipement_chamber.equipement_id
            WHERE equipement_chamber.chamber_id = :chamber_id';

        $stmt = (new Bdd)->getPdo()->prepare( $sql );
        $stmt->execute(['chamber_id' => $chamber_id]);

        $results = $stmt->fetchAll();

        $equipements = [];

        foreach ($results as $result) {
            $equipement = $result;

            $equipements[] = $equipement;
        }
        return $equipements;
    }

}