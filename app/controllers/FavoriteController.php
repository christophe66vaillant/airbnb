<?php

class FavoriteController extends Controller
{
    public function show()
    {
        $user_id = Auth::user()->getId();
        $favorites = Favorite::findAllByUserId( $user_id );
        $this->title = 'Your favorite chambers.';
        $this->render( 'favorite', compact( 'favorites' ) );
    }

    public function addFav( $chamber_id )
    {
        $user_id = Auth::user()->getId();
        if( $user_id && $chamber_id ){
            if(Favorite::checkFav( $user_id, $chamber_id )){
                $this->redirect( '/chamber/' . $chamber_id );

            }$this->addError('Error during insertion');
        }
    }

    public function test()
    {
        $this->title = 'Test';
        $this->render( 'test' );
    }
}