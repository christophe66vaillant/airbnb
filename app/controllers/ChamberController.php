<?php

class ChamberController extends Controller
{
    public function index()
    {
        if( isset($this->fields[ 'select_id' ]) &&  ($this->fields[ 'select_id' ] > 0 || $this->fields[ 'select_id' ] == $this->fields[ 'select_id' ] . ' desc') ){
            $chambers = Chamber::findAll( $this->fields[ 'select_id' ] );
        }else{
            $chambers = Chamber::findAll();
        }
        $this->title = 'Home';
        $this->render( 'chambers', compact( 'chambers' ) );
    }

    public function show( int $chamber_id )
    {
        $images = Picture::findAllById( $chamber_id );
        $chambers = Chamber::findAllById( $chamber_id );
        if( isset( $chambers[0] ) ){
            $this->title = $chambers[0]->getLabel();
        }
        $this->render( 'chamber', compact('chambers', 'images' ) );
    }

    public function showCreate()
    {
        $housings = HousingTypes::findAll();
        $equipements = Equipment::findAll();
        $this->title = 'Create advert';
        $this->render( 'create', compact( 'equipements', 'housings' ) );
    }
    
    public function create()
    {
        if( empty( $_FILES[ 'image' ][ 'name' ] ) || ($pic->inArray() && $_FILES <= $_FILES[4]) ){
            if( $this->checkFields( ['housing_type', 'equipements'] ) > 0){
                if( $this->checkFields( ['country', 'city', 'label', 'description', 'bed', 'size', 'price', 'equipements', 'housing_type'] ) ){
                    $chamber = new Chamber;
                    if( $chamber->createAddress($this->fields[ 'country' ], $this->fields[ 'city' ] ) ){
                        if( $chamber->create($this->fields)){
                            $equipements = $this->fields[ 'equipements' ];
                            if( $chamber->linkEquipements( $equipements ) ){
                                $chamber_id = $chamber->getId();
                                Picture::stockImage( $chamber_id );
                                $this->redirect( '/' );
                            }$this->addError( 'Problem when adding equipements.' );

                        }$this->addError( 'Error during insertion.' );
    
                    }$this->addError( 'Problem when adding your address.' );
    
                }$this->addError( 'Please, fill the fields.' );
    
            }$this->addError( 'Please, choose a housing type.' );

        }$this->addError('Please, choose a valid format of pictures : ' . implode( ', ', $pic->getValid_ext() ) );
        $this->redirect( '/chamber/create' );
    }

    public function showList()
    {
        if( Auth::isLogged() && Auth::user()->hasRole( Role::Advertiser ) ){
            $this->title = 'My rooms';
            $owner_id = $_SESSION[ 'user' ]->getId();
            $chambers = Chamber::findAllByOwnerId( $owner_id );
            $this->render( 'list', compact( 'chambers' ) );
        }
    }

}