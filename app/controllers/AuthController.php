<?php

class AuthController extends Controller
{
    public function form()
    {
        $this->title = 'Authentication';
        $roles = User::findAllRoles();
        $this->render('authentication', compact( 'roles' ));
    }

    public function login()
    {
        if( !empty( $_POST[ 'username' ] ) && !empty($_POST[ 'password' ]) ){
            $auth = new Auth( $username = $_POST[ 'username' ], $password = $_POST[ 'password' ] );
            if( $auth->login() ) $this->redirect( '/' );
            else $this->redirect( '/authentication' );
        }
    }

    public function signin()
    {
        if( !empty( $_POST[ 'username' ] ) && !empty($_POST[ 'password' ]) && $_POST[ 'password_check' ]){
            $auth = new Auth( $username = $_POST[ 'username' ], $password = $_POST[ 'password' ], $_POST[ 'password_check' ] );
            if( !$auth->userExist() ) {
                if($auth->checkPasswords()){
                    if( $_POST[ 'role_id' ] > 0 ){
                        $role = $_POST[ 'role_id' ];
                        if($auth->signin( $role )){
                            $this->redirect( '/' );
                        }else $this->addError('Error during insertion.');
                    }else $this->addError( 'Please choose a role.' );
                }else $this->addError('Please, use the same password in the two fields.');
            }else $this->addError('Sorry, but this username is already taken.');
        }else $this->addError('Please fill all the fields.');
        $this->redirect( '/authentication' );
    }

    public function signout()
    {
        unset($_SESSION[ 'user' ]);
        $this->redirect( '/authentication' );
    }
}