<?php

class ReserveController extends Controller
{
    public function reserve( $id )
    {
        $reserve = new Reserve;
        if( $this->checkFields([ 'start_time', 'end_time']) ){
            if( $reserve->create( $id, $this->fields[ 'start_time' ], $this->fields[ 'end_time' ] ) ){
                $this->addSuccess( 'Your reservation has been written !' );
                $this->redirect( '/chamber/booking' );
            }$this->addError( 'Error during insertion.' );
        }$this->addError( 'Please, enter the time.' );
        $this->redirect( '/chamber/' . $id );
    }

    public function show()
    {
        $owner_id = $_SESSION[ 'user' ]->getId();
        if( $owner_id ){
            $this->title = 'Reserved rooms';
            $reserved = Reserve::findAllByOwnerId($owner_id);
            $this->render( 'booking', compact( 'reserved' ) );
        }$this->addError( 'Sorry, ID not found.' );
        $this->redirect( '/' );
    }

}