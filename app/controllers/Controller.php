<?php 
abstract class Controller 
{
    protected $title = 'Title not defined.';
    protected $errors = [];
    protected $success = [];

    public function __construct()
    {
        if (!empty($_SESSION['errors'])) {
            $this->errors = $_SESSION['errors'];
        }
        if( !empty( $_SESSION[ 'success' ] ) ){
            $this->success = $_SESSION[ 'success' ];
        }
        $this->fields = $_POST;
    }

    protected function checkFields(array $params) : bool
    {
        foreach ($params as $param) {
            if (empty($this->fields[$param]))
            return false;
        }
        return true;
    }

    protected function render( string $view, array $vars = [] ) {

        $vars['title'] = $this->title;
        $vars['errors'] = $this->errors;
        $vars['success'] = $this->success;
        extract( $vars );

        include_once 'public/views/partials/header.php';
        include_once 'public/views/partials/nav.php';
        include_once 'public/views/partials/errors.php';
        include_once 'public/views/partials/success.php';
        include_once "public/views/{$view}.php";
        include_once 'public/views/partials/footer.php';

        unset( $_SESSION[ 'errors' ] );
        unset( $_SESSION[ 'success' ] );
        die;
    }

    protected function addError(string $error)
    {
        $this->errors[] = $error;
        $_SESSION['errors'] = $this->errors;
    }

    protected function addSuccess(string $success)
    {
        $this->success[] = $success;
        $_SESSION['success'] = $this->success;
    }

    protected function notFound() {

        Flight::notFound();
        die;

    }

    protected function redirect( string $route ) {
        
        Flight::redirect( $route );
        die;
        
    }

}