<nav>
    <ul class="d-flex justify-content-around">
        <?php if( Auth::isLogged() && Auth::user()->hasRole( Role::Advertiser ) ): ?>
            <li><a class="btn btn-primary" role="button" href="/">Home</a></li>
            <li><a class="btn btn-primary" role="button" href="/chamber/create">Create chamber</a></li>
            <li><a class="btn btn-primary" role="button" href="/chamber/booking">Reserved rooms</a></li>
            <li><a class="btn btn-primary" role="button" href="/chamber/list">My rooms</a></li>
            <li><a class="btn btn-primary" role="button" href="/signout">Sign out</a></li>
        <?php elseif( Auth::isLogged() && Auth::user()->hasRole( Role::Customer ) ): ?>
            <li><a class="btn btn-primary" role="button" href="/">Home</a></li>
            <li><a class="btn btn-primary" role="button" href="/chamber/booking">My reserved chambers</a></li>
            <li><a class="btn btn-primary" role="button" href="/chamber/favorite">My favorite chambers</a></li>
            <li><a class="btn btn-primary" role="button" href="/signout">Sign out</a></li>
        <?php else: ?>
            <li><a class="btn btn-primary" role="button" href="/">Home</a></li>
            <li><a class="btn btn-primary" role="button" href="/authentication">Login / Register</a></li>
        <?php endif; ?>
    </ul>
</nav>
