<hr>
<?php foreach( $chambers as $chamber ): ?>
    <?php if( isset( $images[0] ) ): ?>
        <div id="pictureChambers" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#pictureChambers" data-slide-to="0" class="active"></li>
                <li data-target="#pictureChambers" data-slide-to="1"></li>
                <?php if( isset( $images[2] ) ): ?>
                    <li data-target="#pictureChambers" data-slide-to="2"></li>
                    <?php if( isset( $images[3] ) ): ?>
                        <li data-target="#pictureChambers" data-slide-to="3"></li>
                        <?php if( isset( $images[4] ) ): ?>
                            <li data-target="#pictureChambers" data-slide-to="4"></li>
                            <?php if( isset( $images[5] ) ): ?>
                                <li data-target="#pictureChambers" data-slide-to="5"></li>
                            <?php endif ?>
                        <?php endif ?>
                    <?php endif ?>
                <?php endif ?>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/public/assets/images/<?php echo $images[0]['image'] ?>" alt="<?php echo $images[0]['alt'] ?>">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="/public/assets/images/<?php echo $images[1]['image'] ?>" alt="<?php echo $images[1]['alt'] ?>">
                </div>
                <?php if( isset( $images[2] ) ): ?>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/public/assets/images/<?php echo $images[2]['image'] ?>" alt="<?php echo $images[2]['alt'] ?>">
                    </div>
                    <?php if( isset( $images[3] ) ): ?>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="/public/assets/images/<?php echo $images[3]['image'] ?>" alt="<?php echo $images[3]['alt'] ?>">
                        </div>
                        <?php if( isset( $images[4] ) ): ?>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="/public/assets/images/<?php echo $images[4]['image'] ?>" alt="<?php echo $images[4]['alt'] ?>">
                            </div>
                            <?php if( isset( $images[5] ) ): ?>
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="/public/assets/images/<?php echo $images[5]['image'] ?>" alt="<?php echo $images[5]['alt'] ?>">
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                    <?php endif ?>
                <?php endif ?>
            </div>
            <a class="carousel-control-prev" href="#pictureChambers" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#pictureChambers" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    <?php endif ?>
        <?php if( Auth::isLogged() && Auth::user()->hasRole( Role::Customer ) ): ?>
            <form action="/chamber/favorite/check/<?php echo $chamber->getId(); ?>" method="post">
                <?php if( Favorite::findAllByUserAndChamberId( Auth::user()->getId(), $chamber->getId() ) ): ?>
                    <button class="btn"><i class="fas fa-heart text-danger"> <i class="text-danger">Delete from your favorite</i></i></button>
                <?php else: ?>
                    <button class="btn"><i class="fas fa-heart text-secondary"> <i class="text-secondary">Add in your favorites</i></i></button>
                <?php endif ?>
            </form>
        <?php endif ?>
        <b>Chamber name : </b><p><?php echo $chamber->getLabel() ?></p>
        <b>Description : </b><p><?php echo $chamber->getDescription() ?></p>
        <b>Price : </b><p><?php echo $chamber->getPrice() ?>€ / nuit</p>
        <b>Location : </b><p><?php echo $chamber->getCountry() ?>, <?php echo $chamber->getCity() ?></p>
        <b>Housing type : </b><p><?php echo $chamber->getHousing_label() ?>, <?php echo $chamber->getBedRooms() ?><?php if( $chamber->getBedRooms() > 1): ?> lits <?php else: ?> lit<?php endif ?></p>
        <?php if( !empty($chamber->getEquipement_label() )): ?><b>Equipements : </b><p><?php echo $chamber->getEquipement_label() ?></p><?php else: ?><b><p>No equipements available.</p></b><?php endif ?>
        <?php if( Auth::isLogged() ): ?>
            <form action="/chamber/booking/<?php echo $chamber->getId() ?>" method="post">
                <div class="text-center"><p>Please choose a date : </p><input type="datetime-local"  name="start_time" value="<?php echo date('Y-m-d\TH:i') ?>"><input type="datetime-local" value="<?php echo date('Y-m-d\TH:i', strtotime( '+1 week' )) ?>" name="end_time"></div>
                <p class="text-center"><b><button type="submit" class="btn btn-outline-primary w-100">Reserve now !</button></b></p>
            </form>
        <?php else: ?>
            <div class="text-center">
                <b><h2>Please connect you or register <a href="/authentication">here</a> to reserve this chamber.</h2></b>
            </div>
        <?php endif ?>
        <hr>
    </div>
<?php endforeach ?>
