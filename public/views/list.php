<hr>
<?php if( Auth::isLogged() && Auth::user()->hasRole( Role::Advertiser ) ): ?>
    <?php if( !empty( $chambers )  ): ?>
        <?php foreach( $chambers as $chamber ): ?>
            <div>
                <b>Chamber name : </b><p><?php echo $chamber->getLabel() ?></p>
                <b>Description : </b><p><?php echo $chamber->getDescription() ?></p>
                <b>Price : </b><p><?php echo $chamber->getPrice() ?>€ / nuit</p>
                <b>Location : </b><p><?php echo $chamber->getCountry() ?>, <?php echo $chamber->getCity() ?></p>
                <b>Housing type : </b><p><?php echo $chamber->getHousing_label() ?>, <?php echo $chamber->getBedRooms() ?><?php if( $chamber->getBedRooms() > 1): ?> lits <?php else: ?> lit<?php endif ?></p>
                <b>Equipements : </b><p><?php echo $chamber->getEquipement_label() ?></p>
                <hr>
            </div>
        <?php endforeach ?>
    <?php else: ?>
        <h1 class="text-center">No chambers available. Click <a href="/chamber/create">here</a> to create one now !</h1>
    <?php endif; ?>
<?php else: ?>
    <h1 class="text-center">You are not authroized to be on this page.</h1>
    <?php Controller::redirect( '/' ) ?>
<?php endif; ?>
