<hr>
<?php if( Auth::isLogged() && !empty( $reserved )): ?>
    <?php foreach ($reserved as $reserve): ?>
        <div>
            <b>Chamber name : </b><p><?php echo $reserve['label'] ?></p>
            <?php if(Auth::isLogged() && Auth::user()->hasRole( Role::Advertiser )): ?><b>By : </b><p><?php echo $reserve[ 'username' ] ?></p>
            <?php elseif( Auth::isLogged() && Auth::user()->hasRole(Role::Customer) ): ?>
                <b>For : </b><p><?php echo $reserve[ 'price' ]?>€ / nuit</p>
                <b>Location : </b><p><?php echo $reserve[ 'city' ] ?>, in <?php echo $reserve[ 'country' ] ?></p>
            <?php endif ?>
            <b>From the </b><?php echo $reserve[ 'start_t' ]; ?> to <?php echo $reserve[ 'end_t' ] ?>
            <hr>
        </div>
    <?php endforeach ?>
<?php elseif( Auth::isLogged() && Auth::user()->hasRole( Role::Advertiser ) ): ?>
    <b><h2 class="text-center">Nobody reserve any one of your chamber.</h2></b>
<?php elseif( Auth::isLogged() && Auth::user()->hasRole( Role::Customer ) ): ?>
    <b><h2 class="text-center">You didn't reserve chamber. Go <a href="/">here to reserve one now</a> !</h2></b>
<?php else: 
    Controller::addError( 'You don\'t have permissions to acces this page.' ); Controller::redirect( '/' );  
?>
<?php endif; ?>