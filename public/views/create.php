<?php if( Auth::isLogged() && Auth::user()->hasRole( Role::Advertiser) ): ?>
    <form action="/chamber/create" method="POST" enctype="multipart/form-data">
    <h1> Create advert : </h1>
        <label>
            <span>Country : </span>
            <input type="text" name="country">
        </label>
        <label>
            <span>City : </span>
            <input type="text" name="city">
        </label>
        <label>
            <span>Name (Little description) : </span>
            <input type="text" name="label">
        </label>
        <label>
            <span>Description : </span>
            <textarea type="text" name="description" placeholder="Discribe your chamber, insert link or other."></textarea>
        </label>
        <label>
            <span>Number of bed : </span>
            <input type="text" name="bed" placeholder="Number of bed.">
        </label>
        <label>
            <span>Size of your chamber : </span>
            <input type="text" name="size" placeholder="Size.">
        </label>
        <label>
            <span>Price : </span>
            <input type="text" name="price" placeholder="Price, for ONE night.">
        </label>
        <div class="select-checkbox">
            <?php foreach ($equipements as $equipement): ?>
                <label>
                    <span><?php echo $equipement[ 'label' ] ?></span>
                    <input type="checkbox" name="equipements[]" value="<?php echo $equipement[ 'id' ] ?>">
                </label>
            <?php endforeach ?>
        </div>
        <label>
            <select name="housing_type">
                <option value="0">Please, choose a house type : </option>
                <?php foreach ($housings as $housing): ?>
                    <option value="<?php echo $housing['id'] ?>"> <?php echo $housing[ 'label' ] ?></option><br>
                <?php endforeach ?>
            </select>
        </label>
        <label>
            <label>Choose one or five pictures : </label>
            <input name="images[]" type="file" multiple>
        </label>
        <button type="submit" class="btn btn-outline-primary">Create now !</button>
    </form>
<?php else: Controller::redirect( '/' ) ?>
<?php endif; ?>