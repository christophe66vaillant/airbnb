<hr>
<div class="row">
    <div class="col-md-6">
        <h2>Login</h2>
        <form action="/login" method="post">
            <label>
                <span>Username : </span>
                <input type="text" name="username">
            </label>
            <label>
                <span>Password : </span>
                <input type="password" name="password">
            </label>
            <button type="submit" class="btn btn-outline-primary">Login</button>
        </form>
    </div>
    
    <div class="col-md-6">
    <div>
        <h2>Sign in </h2>
        <form action="/signin" method="post">
            <label>
                <span>Username : </span>
                <input type="text" name="username">
            </label>
            <label>
                <span>Password : </span>
                <input type="password" name="password">
            </label>
            <label>
                <span>Password (Security) : </span>
                <input type="password" name="password_check">
            </label>
            <label>
                <select name="role_id">
                    <option value="0">Choose a role : </option>
                    <?php foreach ($roles as $role): ?>
                        <option value="<?php echo $role[ 'id' ] ?>"><?php echo $role[ 'name' ] ?></option>
                    <?php endforeach ?>
                </select>
            </label>
            <button type="submit" class="btn btn-outline-primary">Sign in</button>
        </form>
    </div>
</div>
