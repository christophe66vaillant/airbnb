<hr>
<?php if( Auth::isLogged() && !empty( $favorites )): ?>
    <?php foreach ($favorites as $favorite): ?>
        <div>
            <b>Chamber name : </b><p><?php echo $favorite['label'] ?></p>
                <b>For : </b><p><?php echo $favorite[ 'price' ]?>€ / nuit</p>
                <b>Location : </b><p><?php echo $favorite[ 'city' ] ?>, in <?php echo $favorite[ 'country' ] ?></p>
                <b><p>Click <a href="/chamber/<?php echo $favorite[ 'id' ] ?>">here</a> to access at this chamber.</p></b>
            <hr>
        </div>
    <?php endforeach ?>
<?php elseif( Auth::isLogged() && empty( $favorite )): ?>
    <b><h2 class="text-center">You didn't like chamber. Go <a href="/">here to like one now</a> !</h2></b>
<?php else: 
    Controller::addError( 'You don\'t have permissions to acces this page.' ); Controller::redirect( '/' );  
?>
<?php endif; ?>
