<hr>
<?php foreach ($chambers as $chamber): ?>
    <div>
        <b>Chamber name : </b><p><?php echo $chamber->getLabel() ?></p>
        <b>Little description : </b><p><?php echo $chamber->getString_desc() ?>... <a href="/chamber/<?php echo $chamber->getId() ?>">See chambers details.</a></p>
        <b>Price : </b><p><?php echo $chamber->getPrice() ?>€ / nuit</p>
        <b>Location : </b><p><?php echo $chamber->getCountry() ?></p>
        <b>Housing type : </b><p><?php echo $chamber->getHousing_label() ?>, <?php echo $chamber->getBedRooms() ?><?php if( $chamber->getBedRooms() > 1): ?> lits <?php else: ?> lit<?php endif ?>(<?php echo $chamber->getSize() ?>m²)</p>
        <?php if( !empty($chamber->getEquipement_label() )): ?><b>Equipements : </b><p><?php echo $chamber->getEquipement_label() ?></p><?php else: ?><b><p>No equipements available.</p></b><?php endif ?>
        <hr>
    </div>
<?php endforeach ?>
