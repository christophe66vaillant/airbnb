<?php

function airbnbAutoload( $class )
{
    $folders = [
        'app/class',
        'app/class/models',
        'app/controllers'
    ];

    foreach ($folders as $folder) {
        $file = $folder . '/' . $class . '.php';

        if( file_exists( $file ) ){
            require_once $file;
            return;
        }
    }
}

spl_autoload_register( 'airbnbAutoload' );